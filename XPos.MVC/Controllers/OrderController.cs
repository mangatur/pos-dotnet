﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.DataAccess;
using XPos.ViewModel;

namespace XPos.MVC.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectedProduct(long id)
        {
            ProductViewModel product = ProductRepo.ById(id);
            OrderDetailViewModel detail = new OrderDetailViewModel();
            detail.ProductId = product.Id;
            detail.ProductName = product.Name;
            detail.Price = product.Price;
            return PartialView("_SelectedProduct", detail);
        }

        [HttpPost]
        public ActionResult Payment(OrderHeaderViewModel model)
        {
            return PartialView("_Payment", model);
        }

        [HttpPost]
        public ActionResult Pay(OrderHeaderViewModel model)
        {
            ResultOrder result = OrderRepo.Post(model);
            return Json(
                new {
                    success = result.Success,
                    message = result.Message,
                    reference = result.Reference
                }, JsonRequestBehavior.AllowGet);
        }
    }
}