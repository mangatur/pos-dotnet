﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.DataAccess;
using XPos.ViewModel;

namespace XPos.MVC.Controllers
{
    public class VariantController : Controller
    {
        // GET: Variant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<VariantViewModel> ListVariant = VariantRepo.All();
            return PartialView("_List", ListVariant);
        }

        public ActionResult ListByCategory(long id = 0)
        {
            //id => Category Id
            return PartialView("_ListByCategory", VariantRepo.ByCategory(id));
        }

        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(long id)
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            VariantViewModel model = VariantRepo.ById(id);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(long id)
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            VariantViewModel model = VariantRepo.ById(id);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Delete(model.Id);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}