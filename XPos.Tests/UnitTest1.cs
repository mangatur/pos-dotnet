﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using XPos.DataModel;
using System.Linq;
using System.Collections.Generic;
using XPos.ViewModel;
using XPos.DataAccess;

namespace XPos.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCategoryEntity()
        {
            Trace.WriteLine("--- Start Read Category ---");
            using (var db = new XPosContext())
            {
                var listCategory = db.Categories.ToList();
            }
            Trace.WriteLine("--- End Read Category ---");
        }

        [TestMethod]
        public void TestGetAllCategory()
        {
            Trace.WriteLine("--- Start Read Category ---");
            List<CategoryViewModel> cats = CategoryRepo.All();
            foreach (var item in cats)
            {
                Trace.WriteLine(String.Format("Id :{0}, Initial:{1} ", item.Id, item.Initial));
            }
            Trace.WriteLine("--- End Read Category ---");
        }

        [TestMethod]
        public void TestUpdateCategory()
        {
            Trace.WriteLine("--- Start Update Category ---");
            CategoryViewModel cat = new CategoryViewModel();
            cat.Id = 3;
            cat.Initial = "COBAL";
            cat.Name = "Coba-Lagi";
            cat.Active = false;
            // Insert
            ResponseResult result = CategoryRepo.Update(cat);
            if (result.Success)
            {
                Trace.WriteLine("--- Insert Category Success ---");
            } else {
                Trace.WriteLine("--- Insert Category Not Success ---");
            }
            Trace.WriteLine("--- End Update Category ---");
        }

        [TestMethod]
        public void TestInsertCategory()
        {
            Trace.WriteLine("--- Start Insert Category ---");
            using (var db = new XPosContext())
            {
                //var listCategory = db.Categories.ToList();
                Category category = new Category();
                category.Initial = "ManCo";
                category.Name = "Main Course";
                category.Active = true;
                category.CreatedBy = "Atur";
                category.CreatedDate = DateTime.Now;

                db.Categories.Add(category);
                db.SaveChanges();
            }
            Trace.WriteLine("--- End Insert Category ---");
        }

        // Create
        [TestMethod]
        public void TestInsertVariant()
        {
            Trace.WriteLine("--- Start Insert Variant ---");
            using (var db = new XPosContext())
            {
                Variant variant = new Variant();
                variant.CategoryId = 1;
                variant.Initial = "TST";
                variant.Name = "Testing10";
                variant.Active = true;
                variant.CreatedBy = "Atur";
                variant.CreatedDate = DateTime.Now;

                //variant.ModifyBy = "Upin";

                db.Variants.Add(variant);
                db.SaveChanges();
            }
            Trace.WriteLine("--- End Insert Variant ---");
        }

        // Read
        [TestMethod]
        public void TestSelectVariantById()
        {
            Trace.WriteLine("--- Select Variant by Id ---");
            using (var db = new XPosContext())
            {
                Variant variant = db.Variants
                    .Where(v => v.Id == 2)
                    .FirstOrDefault();
                Trace.WriteLine(variant.Initial + " " + variant.Name);
            }
            Trace.WriteLine("--- Select Variant by Id ---");
        }
    }
}
