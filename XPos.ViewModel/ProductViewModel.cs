﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace XPos.ViewModel
{
    public class ProductViewModel
    {
        public long Id { get; set; }

        public long VariantId { get; set; }

        public string VariantName { get; set; }

        public long CategoryId { get; set; }

        public string CategoryName { get; set; }

        [Required, StringLength(10)]
        public string Initial { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal Stock { get; set; }

        [StringLength(20)]
        public string Image { get; set; }

        public HttpPostedFileBase File { get; set; }

        public bool Active { get; set; }
    }
}
